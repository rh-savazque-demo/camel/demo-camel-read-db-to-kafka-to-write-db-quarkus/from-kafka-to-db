package com.redhat.route;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.ListJacksonDataFormat;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.json.JSONObject;
import com.redhat.entity.User;
import org.apache.camel.model.rest.RestBindingMode;
import java.time.LocalDateTime;
//import java.time.format.DateTimeFormatter;


public class KafkaConsumerRoute extends RouteBuilder {

    @Override
    public void configure() throws Exception {

      from("{{kafka.uri}}")
        .log("--- Body ${body} ---")
        .routeId("consumer-route")
        .to("direct:modify-content");

      from("direct:modify-content")
        .process(new Processor() {
          @Override
          public void process(Exchange exchange) throws Exception {

            String user = exchange.getIn().getBody(String.class);
            JSONObject json  = new JSONObject(user);

            User newBody = new User();
            newBody.setId(json.getInt("id"));
            newBody.setName(json.getString("name"));
            newBody.setDate(LocalDateTime.now().toString());

            exchange.getIn().setBody(newBody);

            System.out.println("This is my NEW body: " + exchange.getIn().getBody(String.class));
          }
        })
        .log("--- Body ${body} ---")
        .to("jpa:" + User.class.getName());
    }
}