# Escritura y Lectura de Topico de Kafka con Camel en Quarkus

El proposito de este repositorio es tener una referencia de como generar una ruta de prueba con Apace Camel con Quarkus que pueda leer y escribir datos de un topico de Kafka, de un broker que se encuentra corriendo de forma local. Para este ejemplo se utilizó el generador de proyectos de Quarkus: https://code.quarkus.io/

Extensiones para Consumer:
- Camel Kafka [camel-quarkus-kafka]

Extensiones para Producer:
- Camel Kafka [camel-quarkus-kafka]
- Camel REST [camel-quarkus-rest]
- Camel Direct [camel-quarkus-direct]

Tecnologias utilizadas (versiones comunitarias):
- Apache Maven 3.9.1
- Java 17 (OpenJDK 17.0.9)
- Camel REST 3.6.0
- Camel Direct 3.6.0
- Camel Kafka 3.6.0
- Podman Compose 1.0.6

Imagenes utilizadas:
- docker.io/wurstmeister/kafka:2.13-2.7.0
- docker.io/wurstmeister/zookeeper:latest


## Prueba con kafka corriendo en contenedor local.

### 1. Instalamos Podman Compose
Para crear el broker de Kafka vamos a crar dos contenedores, uno para **zookeeper** y otro para el broker de Kafka propiamente (vease capitulo 5 del libro **Cloud Native integration with Apache Camel** para explicación completa). Para esto, camos a crear estos dos contenedores con Podman Compose y con el archivo `compose.yaml` que se encuentra dentro del directorio `kafka-installation` de este mismo repositorio.

En Fedora, se puede instalar Podman Compose con:
```shell script
sudo dnf install podman-compose

podman-compose --version
```
![Screenshot](assets/readme_images/podman-compose.png)

### 2. Creamos broker de Kafka

Ahora vamos a crear el broker de Kafka ejecutando el `compose.yaml` con Podman Compose. Las imagenes se descargan de `docker.io`.

```shell script
cd kafka-installation/

podman-compose up -d
```
![Screenshot](assets/readme_images/run-compose.png)

Vamos a verificar que los dos contenedores se encuentren corriendo.
```shell script
podman ps | grep wurstmeister
```
![Screenshot](assets/readme_images/containers-running.png)


### 3. Creamos topico de Kafka

Para administrar nuestro broker de Kafka vamos a utilizar los scripts que se encuentran en el directorio `/opt/kafka/bin/`` dentro del filesystem del contenedor `broker`

```shell script
podman exec -it broker /bin/bash

cd /opt/kafka/bin/

ls
```
![Screenshot](assets/readme_images/kafka-scripts.png)

Salimos y vamos a ejecutar los proximos comando sobre los contenedores con `podman exec`.

Entonces, vamos a crear un topico de Kafka con el script `kafka-topics.sh`.Con el siguiente comando:

```shell script
podman exec -it broker /opt/kafka/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 2 --topic example1
```
Este tipico se va a llamar `example1`.

![Screenshot](assets/readme_images/create-topic.png)

Con este mismo script podemos listar todos los topicos que tenemos creados en nuestro broker de Kafka.  

```shell script
podman exec -it broker /opt/kafka/bin/kafka-topics.sh --bootstrap-server localhost:9092 --list
```
![Screenshot](assets/readme_images/topics-list.png)

### 4. Creamos consumidor

Primero vamos a levantar al consumidor. Para esto vamos al directorio `camel-kafka-consumer` y levantamos la ruta en modo desarrollo con Quarkus.

```shell script
cd camel-kafka-consumer

mvn clean compile quarkus:dev -Dkafka.clientid=test -Dkafka.groupid=testGroup
```
![Screenshot](assets/readme_images/consumer-running.png)

### 5. Creamos productor

Ahora para levantar al productor. Para esto vamos al directorio `camel-kafka-producer` y levantamos la ruta en modo desarrollo con Quarkus.

```shell script
cd camel-kafka-producer

mvn clean compile quarkus:dev -Ddebug=5006
```
![Screenshot](assets/readme_images/producer-running.png)

### 6. Consumimos servicio REST

En la ruta del producto se expone el endpoint `/kafka`, por lo que cuando ya tengamos:
1. Broker de Kafka corriendo en contenedores
2. Consumidor corriendo
3. Productor corriendo

Ahora vamos a consumir el endpoint con el siguiente comando:

```shell script
curl -X POST 'http://localhost:8080/kafka' -H 'Content-Type: text/plain' -d 'Testing Kafka'
```
![Screenshot](assets/readme_images/test-rest.png)
